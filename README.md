This tool will be used from nrpe to check smart status on the disks.

The tool recognizes 3ware and LSI raid controllers. It uses smartctl to check the smart data for each drive attached to the RAID controllers.

It has also a version for checking locally attached disks.
