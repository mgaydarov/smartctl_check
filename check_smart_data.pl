#!/usr/bin/perl

use strict;
use warnings;

my $warning = 0;
my $critical = 0;
my $device_data = '';
my $disks_for_change = 0;
my $smartctl_bin = '/usr/sbin/smartctl';

# Structure for known controllers
# 	CLI commands
# 	smartctl options
# 	regular expression for detecting controller numbers
# 	regular expression for detecting port numbers
# 	device type for search in /proc/devices
my $known_controllers = {
	'3ware' => {
		'controller_numbers' => {},
		'device_type' => 'tw',
		'found' => 0,
		'command' => '/usr/sbin/tw-cli',
		'smartctl_options' => '-d 3ware,',
		'regex_controllers' => '^c([0-9]+)\s+',
		'regex_ports' => '^p([0-9]+)\s+'
	},
	'LSI' => {
		'controller_numbers' => {},
		'device_type' =>  'megaraid_sas_ioctl',
		'found' => 0,
		'command' => '/opt/MegaRAID/storcli/storcli64',
		'smartctl_options' => '-d sat+megaraid,',
		'regex_controllers' => '^\s+([0-9]+)\s+',
		'regex_ports' => '^[0-9]+:[0-9]+\s+([0-9]+)\s+'
	}
};

# Struncture holding smart options and their warning and critical levels
my $smart_values = {
	'5' => {
		'name' => 'Reallocated Sectors Count',
		'value' => 0,
		'threshold' => 50
	},
	'10' => {
		'name' => 'Spin Retry Count',
		'value' => 0,
		'threshold' => 10
	},
	'183' => {
		'name' => 'SATA Downshift Error Count or Runtime Bad Block',
		'value' => 0,
		'threshold' => 5
	},
	'184' => {
		'name' => 'End-to-End error / IOEDC',
		'value' => 0,
		'threshold' => 5
	},
	'187' => {
		'name' => 'Reported Uncorrectable Errors',
		'value' => 0,
		'threshold' => 5
	},
	'188' => {
		'name' => 'Command Timeout',
		'value' => 0,
		'threshold' => 10
	},
	'196' => {
		'name' => 'Reallocation Event Count',
		'value' => 0,
		'threshold' => 50
	},
	'197' => {
		'name' => 'Current Pending Sector Count',
		'value' => 0,
		'threshold' => 5
	},
	'198' => {
		'name' => 'Uncorrectable Sector Count or Offline Uncorrectable or Off-Line Scan Uncorrectable Sector Count',
		'value' => 0,
		'threshold' => 5
	},
	'201' => {
		'name' => 'Soft Read Error Rate or TA Counter Detected',
		'value' => 0,
		'threshold' => 50
	},
	'230' => {
		'name' => 'Drive Life Protection Status',
		'value' => 0,
		'threshold' => 5
	},
	'233' => {
		'name' => 'Media Wearout Indicator',
		'value' => 0,
		'threshold' => 5
	}
};

sub unknown_exit {
	# Print error message and exit with status UNKNOWN
	print @_;
	exit(3);
}

sub critical_exit {
	# Print Critical message and exit with status CRITICAL
	print @_;
	exit(2);
}

sub warning_exit {
	# Print Warning message and exit with status WARNING
	print @_;
	exit(1);
}

sub detect_controllers {
	open my $DEVS, '<', '/proc/devices' or unknown_exit("Could not open /proc/devices for reading");
	# Search for known device type in /proc/devices
	while (my $line = <$DEVS>) {
		next unless (defined($line));

		$line =~ s/[\r\n]+$//g;

		next if ($line =~ /^\s*$/);

		while (my ($k, $v) = each(%{$known_controllers})) {
			# Split line by space so we can easily value from second column
			my @splitted_line = split(/\s+/, $line);

			if ($splitted_line[1] =~ /^\Q$v->{'device_type'}\E/) {
				# We've detected known controller
				$v->{'found'} = 1;
			}
		}
	}
	close $DEVS;
}

sub get_controller_numbers {
	while (my ($k, $v) = each(%{$known_controllers})) {
		# If known controller hasn't been found skip it
		next if (! $v->{'found'});

		open my $CMD, '-|', "$v->{'command'} show" or unknown_exit("Could not execute '$v->{'command'} show'");
		while (my $line = <$CMD>) {
			next unless (defined($line));

			$line =~ s/[\r\n]+$//g;

			next if ($line =~ /^\s*$/);

			# Search for controller number
			if ($line =~ /$v->{'regex_controllers'}/) {
				# If we found controller number, prepare structure in which we will hold port and device information
				$v->{'controller_numbers'}->{$1} = {
					'device' => '',
					'ports' => []
				};
			}
		}
		close $CMD;
		if ($? >> 8) {
			unknown_exit("Command '$v->{'command'} show' exited with status " . ($? >> 8));
		}
	}
}

sub map_devices {
	my (@files, @current_numbers);

	if ($known_controllers->{'3ware'}->{'found'}) {
		# Map 3ware controllers
		@current_numbers = sort {$a <=> $b} keys(%{$known_controllers->{'3ware'}->{'controller_numbers'}});

		opendir my $DEV, "/dev/";
		while (my $file = readdir($DEV)) {
			# Find all devices in /dev which are of type /dev/twa*** or /dev/twl***
			if ($file =~ /^tw[al][0-9]+$/) {
				push (@files, $file);
			}
		}
		close $DEV;

		# Sort in human intuitive format founded devices
		@files = sort {
			my @a = $a =~ /(...)(.+)/;
			my @b = $b =~ /(...)(.+)/;
			$a[0] cmp $b[0] or $a[1] <=> $b[1];
		} @files;

		foreach my $file (@files) {
			# Try to open device for reading. If unsuccessfull the device is dummy and we should skip that
			open my $TW_DEV, '<', "/dev/$file" or next;
			my $current_3ware = shift @current_numbers;
			last if (! defined($current_3ware));
			# Store mapping to controller
			$known_controllers->{'3ware'}->{'controller_numbers'}->{$current_3ware}->{'device'} = "/dev/$file";
			close $TW_DEV;
		}
	}

	@files = ();

	if ($known_controllers->{'LSI'}) {
		# Map LSI controller
		my @current_numbers = sort {$a <=> $b} keys(%{$known_controllers->{'LSI'}->{'controller_numbers'}});

		opendir my $DEV, "/dev/";
		while (my $file = readdir($DEV)) {
			# Find all devices in /dev which are of type /dev/sd*
			if ($file =~ /^sd[a-z]$/) {
				push (@files, $file);
			}
		}
		close $DEV;

		@files = sort {$a cmp $b} @files;

		foreach my $file (@files) {
			# Check if device/vendor file is readable
			if (-r "/sys/class/block/$file/device/vendor") {
				open my $TW_DEV, '<', "/sys/class/block/$file/device/vendor" or unknown_exit("Could not open /sys/class/block/$file/device/vendor for reading");
				my $vendor = <$TW_DEV>;
				close $TW_DEV;

				# Check if vendor is LSI
				if (uc($vendor) =~ /LSI|AVAGO/) {
					my $current_lsi = shift @current_numbers;
					last if (! defined($current_lsi));
					# Store mapping to controller
					$known_controllers->{'LSI'}->{'controller_numbers'}->{$current_lsi}->{'device'} = "/dev/$file";
				}
			}
		}
	}
}

sub get_port_numbers {
	while (my ($k, $v) = each(%{$known_controllers})) {
		# Skip known controllers not detected on this machine
		next if (! $v->{'found'});

		while (my ($c, $d) = each(%{$v->{'controller_numbers'}})) {
			open my $CMD, '-|', "$v->{'command'} /c$c show" or unknown_exit("Could not execute '$v->{'command'} /c$c show'");
			while (my $line = <$CMD>) {
				next unless (defined($line));

				$line =~ s/[\r\n]+$//g;

				next if ($line =~ /^\s*$/);

				if ($line =~ /$v->{'regex_ports'}/) {
					# Store all port numbers in the structure
					push(@{$d->{'ports'}}, $1);
				}
			}
			close $CMD;
			if ($? >> 8) {
				unknown_exit("Command '$v->{'command'} /c$c show' exited with status " . ($? >> 8));
			}
		}
	}
}

sub check_disk {
	my $port = shift;
	my $device = shift;
	my $opts = shift;
	my $tmp_dev_data;
	my $tmp_dev_warn = 0;
	my $tmp_dev_crit = 0;

	$tmp_dev_data .= "\tPort: $port\n";

	# Execute smartctl with necessary options
	open my $SMART_DATA_SN, '-|', "$smartctl_bin -i $opts$port $device" or unknown_exit("Could not execute $smartctl_bin -i $opts$port $device");
	while (my $line = <$SMART_DATA_SN>) {
		next unless (defined($line));

		$line =~ s/[\r\n]+$//g;

		next if ($line =~ /^\s*$/);

		if ($line =~ /^Serial Number:/) {
			# Get drive serial number
			$tmp_dev_data .= "\t\t$line";
			last;
		}
	}
	close $SMART_DATA_SN;
	if ($? >> 8) {
		unknown_exit("Command '$smartctl_bin -i $opts$port $device' exited with status " . ($? >> 8));
	}

	$tmp_dev_data .= "\n\t\t\tErrors:\n";

	# Execute smartctl with necessary options
	open my $SMART_DATA, '-|', "$smartctl_bin -A $opts$port $device" or unknown_exit("Could not execute $smartctl_bin -A $opts$port $device");
	while (my $line = <$SMART_DATA>) {
		next unless (defined($line));

		$line =~ s/[\r\n]+$//g;
		$line =~ s/^\s+//g;

		next if ($line =~ /^\s*$/);

		# Check if each S.M.A.R.T. parameter is in warning or critical range
		while(my ($k, $v) = each(%{$smart_values})) {
			if ($line =~ /^$k/) {
				my @splitted_line = split(/\s+/, $line);
				# Get raw value and check it against warning levels
				if ($splitted_line[9] > $v->{'value'} && $splitted_line[9] < $v->{'threshold'}) {
					$warning = 1;
					$tmp_dev_warn = 1;
					$tmp_dev_data .= "\t\t\t\tWARN: $v->{'name'}: $splitted_line[-1]\n";
				} elsif ($splitted_line[9] >= $v->{'threshold'}) {
					# Check if raw value is above critical level
					$critical = 1;
					$tmp_dev_crit = 1;
					$tmp_dev_data .= "\t\t\t\tCRIT: $v->{'name'}: $splitted_line[-1]\n";
				}
			}
		}
	}
	close $SMART_DATA;
	if ($? >> 8) {
		unknown_exit("Command '$smartctl_bin -A $opts$port $device' exited with status " . ($? >> 8));
	}

	$tmp_dev_data .= "\n";

	# If device is in critical or warning state add it to output
	if ($tmp_dev_warn || $tmp_dev_crit) {
		$disks_for_change++;
		$device_data .= $tmp_dev_data;
	}
}

detect_controllers();
get_controller_numbers();
map_devices();
get_port_numbers();

# Check all disks for every 3ware controller found
if ($known_controllers->{'3ware'}->{'found'}) {
	while (my ($k, $v) = each(%{$known_controllers->{'3ware'}->{'controller_numbers'}})) {
		$device_data .= "Controller 3ware $k\n";
		foreach my $port (@{$v->{'ports'}}) {
			check_disk($port, $v->{'device'}, $known_controllers->{'3ware'}->{'smartctl_options'});
		}
		$device_data .= "\n";
	}
}

# Check all disks for every LSI controller found
if ($known_controllers->{'LSI'}->{'found'}) {
	while (my ($k, $v) = each(%{$known_controllers->{'LSI'}->{'controller_numbers'}})) {
		$device_data .= "Controller LSI $k\n";
		foreach my $port (@{$v->{'ports'}}) {
			check_disk($port, $v->{'device'}, $known_controllers->{'LSI'}->{'smartctl_options'});
		}
		$device_data .= "\n";
	}
}

# Print necessary messages and exit with necessary exit codes
if ($critical) {
	critical_exit($device_data);
} elsif ($warning) {
	warning_exit($device_data);
}

# If we arrived here that means all disks are OK.
print "All disks are OK";
exit(0);
